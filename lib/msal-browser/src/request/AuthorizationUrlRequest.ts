/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */

import { CommonAuthorizationUrlRequest } from "@azure/msal-common/browser";

/**
 * This type is deprecated and will be removed on the next major version update
 * @deprecated Will be removed in future version
 */
export type AuthorizationUrlRequest = Omit<
    CommonAuthorizationUrlRequest,
    "requestedClaimsHash" | "platformBroker"
>;
